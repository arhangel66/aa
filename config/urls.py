# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from jones.main.views import Home, Step1, Step2, Step3, DeleteFile, GetFiles, GetCurrentStep, Form1View, \
    Form2View
from jones.users.views import GoToCurrentStep

urlpatterns = [
    # url(r'^old/$', MainView.as_view(), name="home"),
    url(r'^$', Home.as_view(), name="home"),
    # url(r'^$', Login.as_view(template_name='pages/.html'), name="home"),
    url(r'^upload/$', Step1.as_view(), name="step1"),
    url(r'^connect/$', Step2.as_view(), name="step2"),
    url(r'^update/$', Form1View.as_view(), name="form1"),
    url(r'^policy/$', Form2View.as_view(), name="form2"),
    url(r'^thankyou/$', Step3.as_view(), name="step3"),
    url(r'^file/(?P<pk>\d+)/del/$', DeleteFile.as_view(), name='del_file'),
    url(r'^files/$', GetFiles.as_view(), name='get_files'),
    url(r'^current_step/$', GoToCurrentStep.as_view(), name='current_step'),
    url(r'^get_current_step/$', GetCurrentStep.as_view(), name='get_step'),
    # url(r'^$', Step2.as_view(template_name='pages/step2.html'), name="home"),

    # url(r'^$', Step3.as_view(template_name='pages/step3.html'), name="home"),




    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name="about"),

    url(r'^p/', include('jones.main.providers.urls')),
    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),
    # User management
    url(r'^users/', include("jones.users.urls", namespace="users")),
    url(r'^accounts/', include('allauth.urls')),
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT})

    # Your stuff: custom urls includes go her

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
              # + static(
    # settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception("Bad Request!")}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception("Permissin Denied")}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception("Page not Found")}),
        url(r'^500/$', default_views.server_error),
    ]
