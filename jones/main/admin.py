# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin
from .models import *


@admin.register(UploadedFiles)
class UploadedFilesAdmin(admin.ModelAdmin):
    list_display = ('user', 'up_file')
    search_fields = ('user',)
