from django import forms


class DataForm1(forms.Form):
    describe_business = forms.CharField(max_length=1000, required=True, widget=forms.Textarea(),
                                        label='Please describe your business operations in a few sentences.')
    website = forms.URLField(max_length=100, widget=forms.URLInput(), required=True,
                             label='Website')
    subcontracting = forms.CharField(max_length=100, widget=forms.Textarea(), required=True,
                                     label='Does any of your business involve subcontracting work to others?')
    how_many_subcon = forms.CharField(max_length=100, widget=forms.TextInput(), required=True,
                                      label='How many subcontractors?')
    any_subssidiaries = forms.CharField(max_length=100, widget=forms.Textarea(), required=True,
                                        label='Do you have any subsidiaries or own, operate, or have more than 50% ownership '
                                 'interest in a business other than the business described in this application?')
    sold_products = forms.CharField(max_length=100, widget=forms.Textarea(), required=True,
                                    label='Have you sold any new products or services over the last year?')
    revenue = forms.CharField(max_length=100, widget=forms.TextInput(), required=True,
                              label='What will your revenue be for the next fiscal year?')
    estimated_gross = forms.CharField(max_length=100, widget=forms.Textarea(), required=True,
                                      label='During the next 12 months, what are the estimated gross sales you will earn from your'
                                 ' largest customer?')
    any_changes = forms.CharField(max_length=100, widget=forms.Textarea(), required=True,
                                  label='Any changes to your office/building over the last year? (Alarms, roofing, repairs, '
                                 'constructions, etc..)')

    def clean(self):
        cleaned_data = super(DataForm1, self).clean()


        return cleaned_data



class DataForm2(forms.Form):
    past_bakruptcy = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                     label='Have you had a past, pending or planned bankruptcy or judgment for unpaid taxes in the past five years?')
    general_liability = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                        label='Has your company had any General Liability, Property or Worker\'s Compensation claims in the past 5 years?')
    buisiness_insurance = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                          label='Has your business insurance ever been declined, cancelled or non-renewed?')
    change_locations = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                       label='Would you like to change/add locations to your policy?')
    data_coverage = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                    label='Do you want your quote to include Data Breach coverage? (Provides coverage for loss cause by a breach of personally identifiable information)')
    coverage_for_computers = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                             label='Do you want coverage for computers, furniture, stock/inventory or other items owned by your business?')
    coverage_for_mobile = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                          label='Do you want coverage for your mobile property, equipment and trailers?')
    legal_claims = forms.CharField(max_length=1000, widget=forms.Textarea(), required=True,
                                   label='In the past 5 years, have you had any legal claims, lawsuits or actions filed against you?')
