# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from jones.users.models import User


@python_2_unicode_compatible
class UploadedFiles(models.Model):
    user = models.ForeignKey(User, blank=False)
    up_file = models.FileField(_('File'), blank=True, upload_to="files")

    def __str__(self):
        return "%s" % self.up_file.name

    @property
    def size(self):
        return self.up_file.size

    # def get_absolute_url(self):
    #     return reverse('users:detail', kwargs={'username': self.username})
