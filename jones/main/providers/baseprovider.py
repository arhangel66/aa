from abc import ABCMeta

from allauth.socialaccount.models import SocialApp, SocialAccount, SocialToken
from annoying.functions import get_config


class BaseProvider(object):
    def __init__(self, user, name):
        self.name = name
        self.app, cr = SocialApp.objects.get_or_create(name=self.name)
        self.user = user
        self.key = self.app.key
        self.secret = self.app.secret
        self.domain = get_config('DOMAIN')
        self.client = None

    def get_social_account(self):
        account, cr = SocialAccount.objects.get_or_create(user=self.user, provider=self.name, uid=self.user.id)
        return account

    def get_social_token(self):
        token, cr = SocialToken.objects.get_or_create(app=self.app, account=self.get_social_account())
        return token

    def save_to_db(self, stored_values):
        expires = None
        if 'oauth_expires_at' in stored_values:
            expires = stored_values.pop('oauth_expires_at')
        if 'oauth_authorization_expires_at' in stored_values:
            expires = stored_values.pop('oauth_authorization_expires_at')

        # save social_account
        account = self.get_social_account()
        account.extra_data = stored_values
        account.save()

        # save social_token
        token = self.get_social_token()
        if stored_values.get('oauth_token', ''):
            token.token = stored_values.get('oauth_token', '')
            token.token_secret = stored_values.get('oauth_token_secret', '')
        token.expires_at = expires
        token.save()
        return True

    def get_from_db(self):
        account = self.get_social_account()
        stored_values = account.extra_data

        token = self.get_social_token()
        stored_values['oauth_expires_at'] = token.expires_at
        stored_values['oauth_authorization_expires_at'] = token.expires_at
        return stored_values
