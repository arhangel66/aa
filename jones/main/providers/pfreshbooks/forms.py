from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms


class GetSiteForm(forms.Form):
    site = forms.CharField(max_length=50, label='Site', widget=forms.TextInput(
        attrs={'placeholder': 'Your site'}))

    def __init__(self, *args, **kwargs):
        super(GetSiteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/p/fresh/login/'
        self.helper.add_input(Submit('submit', 'Submit'))


    def clean(self):
        cleaned_data = super(GetSiteForm, self).clean()
        site = cleaned_data.get("site")
        subdomain = ''

        should_be = ['http', 'freshbooks']
        remove_list = ['https://', 'http://', '']

        if 'http' in site and 'freshbooks' in site:
            site = site.replace('http://', '').replace('https://', '')
            if '.' in site:
                subdomain = site.split('.')[0]
        else:
            if not '.' in site and not '/' in site:
                subdomain = site

        if not subdomain:
            raise forms.ValidationError(
                    "You should set url of your freshbooks account, like https://sky6495.freshbooks.com"
                )
        else:
            cleaned_data['subdomain'] = subdomain
        return cleaned_data

