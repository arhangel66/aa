import time
from urlparse import parse_qs

import requests
import xlwt
from django.core.urlresolvers import reverse
from jones.main.providers.baseprovider import BaseProvider
from jones.main.utils import add_sheet_to_wb, save_wb
from refreshbooks import api


def gen_nonce():
    nonce = ''
    import string
    import random
    for i in range(20):
        nonce += random.choice(string.letters)
    return nonce


def get_list_from_dict_freshbooks(mydict):
    my_list = []
    my_list.append(mydict[0].__dict__.keys())
    for item in mydict:
        my_list.append(item.__dict__.values())
    return my_list


class PFreshBooks(BaseProvider):
    def __init__(self, user):
        super(PFreshBooks, self).__init__(user, 'freshbooks')

    def gen_login_url(self, subdomain):
        stored_values = {
            'oauth_consumer_key': self.key,
            'oauth_callback': '%s%s' % (self.domain, reverse('fresh_callback')),
            'oauth_signature': self.secret + '&',
            'oauth_signature_method': 'PLAINTEXT',
            'oauth_version': '1.0',
            'oauth_timestamp': int(time.time()),
            'oauth_nonce': gen_nonce(),
            'subdomain': subdomain
        }
        url = "https://%s.freshbooks.com/oauth/oauth_request.php" % subdomain
        session = requests.session()
        response = session.post(url, stored_values)
        resp = parse_qs(response.text)

        if resp.get('oauth_token'):
            confirm_url = "https://%s.freshbooks.com/oauth/oauth_authorize.php?oauth_token=%s" % (
                subdomain, resp.get('oauth_token')[0])

            self.save_to_db(stored_values)
            return confirm_url
        else:
            print '51 some errors', resp

    def callback(self, get_params):
        params = self.get_from_db()
        subdomain = params.get('subdomain')
        params['oauth_verifier'] = get_params['oauth_verifier']
        params['oauth_token'] = get_params['oauth_token']
        params['oauth_nonce'] = gen_nonce()
        url = "https://%s.freshbooks.com/oauth/oauth_access.php" % subdomain

        session = requests.session()
        response = session.post(url, params)
        resp = parse_qs(response.text)

        # {u'oauth_token_secret': [u'5KAR77GUiUHhryrRwvjTcdhfKp7qzfiMc'], u'oauth_token': [u'3dVPE7rrKRv68cGmG...V']}
        print resp
        token = self.get_social_token()
        token.token = resp.get('oauth_token')[0]
        token.token_secret = resp.get('oauth_token_secret')[0]
        token.save()
        return True

    def get_client(self):
        params = self.get_from_db()
        subdomain = params.get('subdomain')
        token = self.get_social_token()
        client = api.OAuthClient(
            '%s.freshbooks.com' % subdomain,
            self.key,  # my key
            self.secret,  # my secret
            token.token,  # get key
            token.token_secret)  # get secret
        self.client = client
        return client

    def get_and_save_info(self):
        fresh = self.get_client()
        try:
            clients_list = get_list_from_dict_freshbooks(fresh.client.list().clients.client)
        except:
            clients_list = []

        try:
            expenses_list = get_list_from_dict_freshbooks(fresh.expense.list().expenses.expense)
        except:
            expenses_list = []

        try:
            payments_list = get_list_from_dict_freshbooks(fresh.payment.list().payments.payment)
        except:
            payments_list = []

        try:
            contractors_list = get_list_from_dict_freshbooks(fresh.contractor.list().contractors.contractor)
        except:
            contractors_list = []

        try:
            staff_list = get_list_from_dict_freshbooks(fresh.staff.list().staff_members.member)
        except:
            staff_list = []

        wb = xlwt.Workbook()
        wb = add_sheet_to_wb(clients_list, 'clients', wb)
        wb = add_sheet_to_wb(expenses_list, 'expenses', wb)
        wb = add_sheet_to_wb(payments_list, 'payments', wb)
        wb = add_sheet_to_wb(staff_list, 'staff', wb)
        wb = add_sheet_to_wb(contractors_list, 'contractors', wb)
        save_wb(self.user, wb, self.name)
