
# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include
from jones.main.providers.pfreshbooks.views import FreshLogin, FreshCallBack, FreshInfo

urlpatterns = [
    # URL pattern for the UserListView
    url(r'^login/', FreshLogin.as_view(), name='fresh_login'),
    url(r'^callback/', FreshCallBack.as_view(), name='fresh_callback'),
    url(r'^info/', FreshInfo.as_view(), name='fresh_info'),
]
