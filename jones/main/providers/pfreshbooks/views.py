from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import View, TemplateView, FormView
from jones.main.providers.pfreshbooks.forms import GetSiteForm
from jones.main.providers.pfreshbooks.pfreshbooks import PFreshBooks


class FreshLogin(LoginRequiredMixin, FormView):
    """
    Get login url and redirect to login page
    """
    form_class = GetSiteForm
    template_name = 'freshbooks/get_site.html'

    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        subdomain = form.cleaned_data.get('subdomain')
        return HttpResponseRedirect(self.get_redirect_url(subdomain))

    def get_redirect_url(self, subdomain):
        provider = PFreshBooks(self.request.user)
        return provider.gen_login_url(subdomain=subdomain)


class FreshCallBack(View):
    """
    Get answer from server and save new params
    """

    def get(self, *args, **kwargs):
        """
        ?oauth_verifier=hsUvx2YJw2Us8a9hLh4kDrH9tVpwjYxXG&oauth_token=hgY5cUcCfKVaiEr6fJR4kxS7naqVvzKP8
        :param args:
        :param kwargs:
        :return:
        """
        provider = PFreshBooks(self.request.user)
        callback_result = provider.callback(self.request.GET)
        if callback_result:
            return HttpResponseRedirect(reverse('fresh_info'))


class FreshInfo(TemplateView):
    """
    Read params for connect from DB and connect to Xero for reading info (clients, invoices, etc)
    """
    template_name = 'pages/fresh_info.html'

    def get_context_data(self, **kwargs):
        context = super(FreshInfo, self).get_context_data(**kwargs)
        provider = PFreshBooks(self.request.user)
        provider.get_and_save_info()

        self.request.user.current_step = 'step3'
        self.request.user.save()

        return context
