import time
from urlparse import parse_qs

import requests
from django.core.urlresolvers import reverse
from jones.main.providers.baseprovider import BaseProvider
from quickbooks import QuickBooks
from quickbooks.objects.customer import Customer
from refreshbooks import api


class PQuickBooks(BaseProvider):
    def __init__(self, user):
        super(PQuickBooks, self).__init__(user, 'quickbooks')

    def get_callback_url(self):
        return '%s%s' % (self.domain, reverse('quick_callback'))

    def gen_login_url(self):
        quickbooks = QuickBooks(
            # sandbox=True,
            consumer_key=self.key,
            consumer_secret=self.secret,
            callback_url=self.get_callback_url()
        )
        print self.key, self.secret, '%s%s' % (self.domain, reverse('quick_callback'))

        authorize_url = quickbooks.get_authorize_url()

        token = self.get_social_token()
        token.token = quickbooks.request_token
        token.token_secret = quickbooks.request_token_secret
        token.save()
        self.save_to_db({'authorize_url': authorize_url})
        print authorize_url
        return authorize_url

    def callback(self, get_params):
        quickbooks = QuickBooks(
            # sandbox=True,
            consumer_key=self.key,
            consumer_secret=self.secret,
            callback_url=self.get_callback_url()
        )
        info = self.get_from_db()
        token = self.get_social_token()
        quickbooks.authorize_url = info.get('authorize_url')
        quickbooks.request_token = token.token
        quickbooks.request_token_secret = token.token_secret
        quickbooks.set_up_service()
        print 50, get_params, get_params.keys()
        quickbooks.get_access_tokens(get_params['oauth_verifier'])

        info.update({'realmId': get_params['realmId']})
        self.save_to_db(info)

        return True

    def get_client(self):
        token = self.get_social_token()
        data = self.get_from_db()
        client = QuickBooks(
            sandbox=True,
            consumer_key=self.key,
            consumer_secret=self.secret,
            access_token=token.token,
            access_token_secret=token.token_secret,
            company_id=data.get('realm_id'),
        )
        # print self.qb.query('SELECT * FROM Customer  MAXRESULTS 100')
        # self.customer = Customer()
        # print 71, self.customer
        # print 72, self.customer.all()
        # from quickbooks.objects.customer import Customer
        # customers = Customer.all()
        print client
