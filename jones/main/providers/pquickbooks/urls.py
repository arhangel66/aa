# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include
from jones.main.providers.pquickbooks.views import QuickLogin, QuickCallBack, QuickInfo

urlpatterns = [
    # URL pattern for the UserListView
    url(r'^login/', QuickLogin.as_view(), name='quick_login'),
    url(r'^callback/', QuickCallBack.as_view(), name='quick_callback'),
    url(r'^info/', QuickInfo.as_view(), name='quick_info'),
]


