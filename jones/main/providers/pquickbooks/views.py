from urlparse import parse_qs

import requests
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import RedirectView, View, TemplateView
from jones.main.providers.pquickbooks.pquickbooks import PQuickBooks


class QuickLogin(RedirectView):
    """
    Get login url and redirect to login page
    """

    def get_redirect_url(self, *args, **kwargs):
        provider = PQuickBooks(self.request.user)
        return provider.gen_login_url()


class QuickCallBack(View):
    """
    Get answer from server and save new params
    """

    def get(self, *args, **kwargs):
        """
        ?oauth_verifier=hsUvx2YJw2Us8a9hLh4kDrH9tVpwjYxXG&oauth_token=hgY5cUcCfKVaiEr6fJR4kxS7naqVvzKP8
        :param args:
        :param kwargs:
        :return:
        """
        provider = PQuickBooks(self.request.user)
        callback_result = provider.callback(self.request.GET)


        if callback_result:
            return HttpResponseRedirect(reverse('quick_info'))


class QuickInfo(TemplateView):
    """
    Read params for connect from DB and connect to Xero for reading info (clients, invoices, etc)
    """
    template_name = 'pages/quick_info.html'

    def get_context_data(self, **kwargs):
        context = super(QuickInfo, self).get_context_data(**kwargs)
        provider = PQuickBooks(self.request.user)
        client = provider.get_client()
        self.request.user.current_step = 'step3'
        self.request.user.save()

        return context
