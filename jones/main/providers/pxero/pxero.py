import datetime

import xlwt
from django.core.urlresolvers import reverse
from jones.main.providers.baseprovider import BaseProvider
from jones.main.utils import get_list_from_dict, save_wb, add_sheet_to_wb
from xero import Xero
from xero.auth import PublicCredentials
from xero.exceptions import XeroException


class PXero(BaseProvider):
    def __init__(self, user):
        super(PXero, self).__init__(user, 'xero')

    def gen_login_url(self):
        stored_values = {}
        credentials = PublicCredentials(
            self.key, self.secret, callback_uri='%s%s' % (self.domain, reverse('xero_callback')))

        # Save generated credentials details to persistent storage
        for key, value in credentials.state.items():
            stored_values.update({key: value})

        self.save_to_db(stored_values)

        return credentials.url

    def get_client(self):
        stored_values = self.get_from_db()
        credentials = PublicCredentials(**stored_values)

        try:
            xero = Xero(credentials)
            return xero
        except XeroException as e:
            print ('{}: {}'.format(e.__class__, e.message))
            return None

    def get_and_save_info(self):
        last_year = datetime.date.today() - datetime.timedelta(days=365)
        xero = self.get_client()
        invoices = xero.invoices.filter(since=last_year)
        invoices_list = get_list_from_dict(invoices)

        contacts = xero.contacts.all()
        contacts_list = get_list_from_dict(contacts)

        employees = xero.employees.all()
        employees_list = get_list_from_dict(employees)

        wb = xlwt.Workbook()
        wb = add_sheet_to_wb(invoices_list, 'invoices', wb)
        wb = add_sheet_to_wb(contacts_list, 'contacts', wb)
        wb = add_sheet_to_wb(employees_list, 'employees', wb)
        save_wb(self.user, wb, self.name)
        return True
