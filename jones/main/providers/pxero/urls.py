# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include
from jones.main.providers.pxero.views import LoginProv, CallBack, XeroInfo

urlpatterns = [
    # URL pattern for the UserListView
    url(r'^login/', LoginProv.as_view(), name='xero_login'),
    url(r'^callback/', CallBack.as_view(), name='xero_callback'),
    url(r'^info/', XeroInfo.as_view(), name='xero_info'),
]
