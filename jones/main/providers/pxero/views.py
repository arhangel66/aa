
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import RedirectView, View, TemplateView
from jones.main.providers.pxero.pxero import PXero
from xero.auth import PublicCredentials
from xero.exceptions import XeroException


class LoginProv(RedirectView):
    """
    Get login url and redirect to login page
    """

    def get_redirect_url(self, *args, **kwargs):
        provider = PXero(user=self.request.user)
        return provider.gen_login_url()


class CallBack(View):
    """
    Get answer from server and save new params
    """

    def get(self, *args, **kwargs):
        params = self.request.GET
        if 'oauth_token' not in params or 'oauth_verifier' not in params or 'org' not in params:
            return HttpResponse('Missing parameters required.')

        provider = PXero(user=self.request.user)
        stored_values = provider.get_from_db()
        credentials = PublicCredentials(**stored_values)

        try:
            credentials.verify(params['oauth_verifier'])

            # Resave our verified credentials
            for key, value in credentials.state.items():
                stored_values.update({key: value})
            provider.save_to_db(stored_values)

        except XeroException as e:
            return HttpResponse('error', '{}: {}'.format(e.__class__, e.message))

        # Once verified, api can be invoked with xero = Xero(credentials)
        return HttpResponseRedirect(reverse('xero_info'))


class XeroInfo(TemplateView):
    """
    Read params for connect from DB and connect to Xero for reading info (clients, invoices, etc)
    """
    template_name = 'pages/xero_info.html'

    def get_context_data(self, **kwargs):
        print 59
        context = super(XeroInfo, self).get_context_data(**kwargs)
        # xero = self.get_xero()
        provider = PXero(user=self.request.user)
        provider.get_and_save_info()
        self.request.user.current_step = 'step3'
        self.request.user.save()
        print 64
        return context
