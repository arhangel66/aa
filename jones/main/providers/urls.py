# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include

urlpatterns = [
    # URL pattern for the UserListView
    url(r'^xero/', include('jones.main.providers.pxero.urls')),
    url(r'^fresh/', include('jones.main.providers.pfreshbooks.urls')),
    url(r'^quick/', include('jones.main.providers.pquickbooks.urls')),
]
