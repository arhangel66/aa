import StringIO

from django.core.files import File


def get_list_from_dict(mydict):
    my_list = []
    if mydict:
        my_list.append(mydict[0].keys())
        for item in mydict:
            my_list.append(item.values())
    return my_list


def add_sheet_to_wb(data, sheet_name, wb):
    ws = wb.add_sheet(sheet_name)
    for x, line in enumerate(data):
        for y, val in enumerate(line):
            val = "%s" % val
            ws.write(x, y, val)
    return wb


def save_wb(user, wb, name='new'):
    import datetime
    f = StringIO.StringIO()
    wb.save(f)
    file_name = '%s-%s.xls' % (name, datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    user.from_api.save(file_name, File(f))
    user.save()
