from allauth.account.views import SignupView
from braces.views import LoginRequiredMixin, CsrfExemptMixin, JSONResponseMixin
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, DeleteView, View, FormView
from jones.main.forms import DataForm1, DataForm2
from jones.main.models import UploadedFiles


class MainView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['quick_href'] = reverse('quick_login')
        context['xero_href'] = reverse('xero_login')
        context['fresh_href'] = reverse('fresh_login')
        return context


class SaveStepMixin(object):
    name = 'test'

    steps = ['step1', 'step2', 'form1', 'form2', 'step3']

    def get_step_number(self, step):
        if step and step in self.steps:
            return self.steps.index(step)
        return -1

    def get_context_data(self, **kwargs):
        if self.request.user.id:
            if self.get_step_number(self.request.user.current_step) < self.get_step_number(self.name):
                self.request.user.current_step = self.name
                self.request.user.save()
        context = super(SaveStepMixin, self).get_context_data(**kwargs)
        return context


class Home(SignupView):
    template_name = "pages/landing.html"
    title = "WhyJones"
    # template_name = 'jones_base.html'




class Step1(LoginRequiredMixin, SaveStepMixin, TemplateView):
    template_name = 'pages/step1.html'
    name = 'step1'

    def get_context_data(self, **kwargs):
        context = super(Step1, self).get_context_data(**kwargs)
        context['result'] = [{'size': 1112222, 'name': 'test_name.pgd'}]

    def post(self, request):
        if self.request.FILES and 'file' in self.request.FILES:
            upfile, cr = UploadedFiles.objects.get_or_create(user=self.request.user, up_file=self.request.FILES['file'])
            return HttpResponse(upfile.id)
        return HttpResponse('No')


class DeleteFile(CsrfExemptMixin, DeleteView):
    model = UploadedFiles
    success_url = '/'

    def post(self, request, *args, **kwargs):
        if self.get_object().user == self.request.user:
            super(DeleteFile, self).post(request, *args, **kwargs)
            return HttpResponse('Ok')
        return HttpResponse('No')


class GetFiles(LoginRequiredMixin, JSONResponseMixin, View):
    def get(self, request):
        return self.render_json_response(context_dict=self.get_data(), status=200)

    def get_data(self):
        files = UploadedFiles.objects.filter(user=self.request.user)
        files_list = []
        for file in files:
            files_list.append({
                'name': file.up_file.name,
                'size': file.up_file.size,
                'serverId': file.id
            })
        return files_list


class Step2(LoginRequiredMixin, SaveStepMixin, TemplateView):
    template_name = 'pages/step2.html'
    name = 'step2'

    def get_context_data(self, **kwargs):
        context = super(Step2, self).get_context_data(**kwargs)
        context['quick_href'] = reverse('quick_login')
        context['xero_href'] = reverse('xero_login')
        context['fresh_href'] = reverse('fresh_login')
        return context


class Step3(LoginRequiredMixin, SaveStepMixin, TemplateView):
    template_name = 'pages/step3.html'
    name = 'step3'


def prepare_text(form):
    text = ""
    for field in form:
        value = form.cleaned_data.get(field.name)
        text += "%s\n%s\n\n" % (field.label, value)
    return text


class Form1View(LoginRequiredMixin, SaveStepMixin, FormView):
    template_name = 'pages/form1.html'
    form_class = DataForm1
    name = 'form1'
    success_url = reverse_lazy('form2')

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
        }
        post_dict = {}

        for key in self.request.POST:
            value = filter(None, self.request.POST.getlist(key))
            if not value:
                value = ['No']
            post_dict[key] = value[0]

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': post_dict,
                'files': self.request.FILES,
            })
        print 143, kwargs
        return kwargs

    def form_valid(self, form):
        self.request.user.form1_data = prepare_text(form)
        self.request.user.save()

        return HttpResponseRedirect(self.get_success_url())


class Form2View(Form1View):
    template_name = 'pages/form2.html'
    form_class = DataForm2
    name = 'form2'
    success_url = reverse_lazy('step3')

    def form_valid(self, form):
        self.request.user.form2_data = prepare_text(form)
        self.request.user.save()

        return HttpResponseRedirect(self.get_success_url())


class GetCurrentStep(LoginRequiredMixin, View):
    def get(self, request):
        return HttpResponse(self.request.user.current_step)
