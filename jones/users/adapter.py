# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from jones.users.forms import password_check


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)


    def clean_password(self, password, user=None):
        """
        Validates a password. You can hook into this if you want to
        restric the allowed password choices.
        """
        password = super(AccountAdapter, self).clean_password(password)
        p_check = password_check(password)
        if not p_check.get('password_ok'):
            raise forms.ValidationError("Your password should consist: Upper case, number & symbol")
        return password

class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, social=None):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)
