import re

from django import forms
from django.utils.translation import ugettext_lazy as _
from jones.users.models import string_to_int


class SignupForm(forms.Form):
    phone = forms.CharField(max_length=30, label='Phone', widget=forms.TextInput(
                                   attrs={'placeholder':
                                          _('Phone')}))

    def signup(self, request, user):
        user.phone = self.cleaned_data['phone']
        user.save()


    def clean_phone(self):
        phone = str(string_to_int(self.cleaned_data['phone']))
        print 52, phone
        if len(phone) == 10:
            return phone
        else:
            raise forms.ValidationError("Please enter 10 digits phone number!")




def password_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """

    # calculating the length
    length_error = len(password) < 8

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None

    # overall result
    password_ok = not ( length_error or digit_error or uppercase_error or lowercase_error or symbol_error )

    return {
        'password_ok' : password_ok,
        'length_error' : length_error,
        'digit_error' : digit_error,
        'uppercase_error' : uppercase_error,
        'lowercase_error' : lowercase_error,
        'symbol_error' : symbol_error,
    }
