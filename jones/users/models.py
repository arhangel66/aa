# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


def string_to_int(text):
    """
    Превращает текст в int
    """
    import re
    intval = 0
    clean_text = re.sub("[^0-9]", "", ("%s" % text).strip())
    if "%s" % text.strip()[0] == '-':
        clean_text = '-' + clean_text

    if text:
        intval = int(round(float(clean_text)))



    return intval

@python_2_unicode_compatible
class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    phone = models.CharField(_("Phone"), blank=True, max_length=255)
    current_step = models.CharField(_("current_step"), blank=True, max_length=100)
    form1_data = models.TextField(blank=True, null=True, default='')
    form2_data = models.TextField(blank=True, null=True, default='')
    from_api = models.FileField(blank=True, null=True, upload_to='from_api/')

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def get_last(self):
        if self.current_step:
            return self.current_step
        else:
            return 'step1'

    def get_last_url(self):
        return reverse(self.get_last())


